package com.ef.dao;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import com.ef.utils.DateTimeUtils;

public class AccessLogDao {

	private final static String dbUrl = "jdbc:mysql://213.222.211.195:3306/parser?rewriteBatchedStatements=true&max_allowed_packet=10000000";
	private final static String dbUsername = "root";
	private final static String dbPassword = "TFFfbi64636";
	private final static String SQL_INSERT_ACCESSLOGS = "insert into accesslogs (ip, date, request, status, user_agent) values (?, ?, ?, ?, ?)";
	private final static String SQL_INSERT_BLOCKED_IP = "insert into blocked_ips (ip, comment) values (?, ?)";
	private final static String SQL_SELECT_ACCESSLOGS = "select ip, count(*) as count from accesslogs where date between ? and ? group by ip having count > ?";

	public void clearTable(String tableName) throws SQLException {
		System.out.println("clearing DB ... ");
		Connection conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
		String sql = "DELETE FROM " + tableName;
		PreparedStatement statement = conn.prepareStatement(sql);
		statement.executeUpdate();
		statement.close();
		conn.close();
		System.out.println("done");
		System.out.println();
	};

	public void saveLog(String accesslogPath) throws SQLException, NumberFormatException, IOException {
		System.out.println("parsing logs and saving to DB ... ");
		Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
		PreparedStatement statement = connection.prepareStatement(SQL_INSERT_ACCESSLOGS);

		BufferedReader reader = new BufferedReader(new FileReader(accesslogPath));
		String line = null;
		int i = 0;
		long start = System.currentTimeMillis();
		while ((line = reader.readLine()) != null) {
			String[] log = line.split("\\|");
			statement.setString(1, log[1]);
			statement.setTimestamp(2, DateTimeUtils.toTimestamp(log[0]));
			statement.setString(3, log[2]);
			statement.setInt(4, Integer.valueOf(log[3]));
			statement.setString(5, log[4]);
			statement.addBatch();
			i++;

			if (i % 1000 == 0) {
				statement.executeBatch(); // Execute every 1000 items.
			}
		}
		reader.close();

		statement.executeBatch();
		statement.close();
		connection.close();
		System.out.println("done. Time taken - " + (System.currentTimeMillis() - start) + " milliseconds");
		System.out.println();
	}

	public Map<String, Integer> findIps(LocalDateTime startDate, Duration duration, int threshold) throws SQLException {
		Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);

		PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ACCESSLOGS);
		statement.setTimestamp(1, Timestamp.valueOf(startDate));
		statement.setTimestamp(2, Timestamp.valueOf(startDate.plusSeconds(duration.getSeconds())));
		statement.setInt(3, threshold);

		ResultSet results = statement.executeQuery();
		Map<String, Integer> blockedIps = new HashMap<String, Integer>();
		while (results.next()) {
			String ip = results.getString("ip");
			int count = results.getInt("count");
			blockedIps.put(ip, count);
		}
		statement.close();
		connection.close();
		return blockedIps;
	}

	public void saveBlockedIps(Map<String, Integer> blockedIps, LocalDateTime startDate, Duration duration, Integer threshold) throws SQLException {
		System.out.println("saving blocked IPs to DB ... ");
		Connection connection = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
		PreparedStatement statement = connection.prepareStatement(SQL_INSERT_BLOCKED_IP);

		String comment = String.format("There was more then %d requests from %s till %s", threshold, startDate, startDate.plusSeconds(duration.getSeconds()));
		for (Map.Entry<String, Integer> entry : blockedIps.entrySet()) {
			statement.setString(1, entry.getKey());
			statement.setString(2, comment);
			statement.addBatch();
		}

		statement.executeBatch();
		statement.close();
		connection.close();
		System.out.println("done");
		System.out.println();
	}

}
