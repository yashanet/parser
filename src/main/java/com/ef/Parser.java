package com.ef;

import static java.lang.System.exit;

import com.ef.service.ParserService;

public class Parser {

	public static void main(String[] args) {
		try {

			ArgumentsParser arguments = new ArgumentsParser(args);

			System.out.println("--- arguments ---");
			System.out.println("accesslogPath : " + arguments.getAccesslogPath());
			System.out.println("startDate : " + arguments.getStartDate());
			System.out.println("duration : " + arguments.getDuration());
			System.out.println("threshold : " + arguments.getThreshold());
			System.out.println("------");
			System.out.println();

			ParserService service = new ParserService();

			service.saveAccessLog(arguments.getAccesslogPath());
			service.findAndPrintIPs(arguments.getStartDate(), arguments.getDuration(), arguments.getThreshold());

			System.out.println();
			System.out.println("--- finish ---");
			exit(0);
		} catch (Exception e) {
			System.out.println();
			System.out.println("ERROR: " + e.getMessage());
			System.out.println("--- finish ---");
		}
	}

}
