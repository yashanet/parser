package com.ef.utils;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {

	public static DateTimeFormatter argumentsFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd.HH:mm:ss");
	public static DateTimeFormatter logTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");

	public static Timestamp toTimestamp(String time) {
		return Timestamp.valueOf(LocalDateTime.from(DateTimeUtils.logTimeFormatter.parse(time)));
	}

	public static LocalDateTime toArgumentsTime(String time) {
		return LocalDateTime.from(argumentsFormatter.parse(time));
	}

}
