package com.ef.service;

import java.io.IOException;
import java.sql.SQLException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;

import com.ef.dao.AccessLogDao;

public class ParserService {

	private final AccessLogDao accessLogDao = new AccessLogDao();

	public void saveAccessLog(String accesslogPath) throws SQLException, NumberFormatException, IOException {
		if (accesslogPath == null) {
			return;
		}
		accessLogDao.clearTable("accesslogs");
		accessLogDao.saveLog(accesslogPath);
	}

	public void findAndPrintIPs(LocalDateTime startDate, Duration duration, Integer threshold) throws SQLException {
		Map<String, Integer> blockedIps = accessLogDao.findIps(startDate, duration, threshold);

		System.out.println("IPs to block");
		for (Map.Entry<String, Integer> entry : blockedIps.entrySet()) {
			System.out.println("IP - " + entry.getKey() + ", count = " + entry.getValue());
		}
		System.out.println();

		accessLogDao.saveBlockedIps(blockedIps, startDate, duration, threshold);
	};

}
