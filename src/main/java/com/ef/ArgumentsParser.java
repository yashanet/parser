package com.ef;

import java.time.Duration;
import java.time.LocalDateTime;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import com.ef.utils.DateTimeUtils;

public class ArgumentsParser {

	private String accesslogPath;
	private LocalDateTime startDate;
	private Duration duration;
	private Integer threshold;

	public ArgumentsParser(String[] args) throws Exception {
		Option accesslogOption = new Option("accesslog", "accesslog", true, "accesslog Path");
		accesslogOption.setArgs(1);
		accesslogOption.setArgName("accesslog ");

		Option startDateOption = new Option("startDate", "startDate", true, "Start Date");
		startDateOption.setArgs(1);
		startDateOption.setArgName("startDate ");

		Option durationOption = new Option("duration", "duration", true, "Duration");
		durationOption.setArgs(1);
		durationOption.setArgName("duration ");

		Option thresholdOption = new Option("threshold", "threshold", true, "Hhreshold");
		thresholdOption.setArgs(1);
		thresholdOption.setArgName("threshold ");

		Options opts = new Options();
		opts.addOption(accesslogOption);
		opts.addOption(startDateOption);
		opts.addOption(durationOption);
		opts.addOption(thresholdOption);

		CommandLineParser cmdParser = new DefaultParser();
		CommandLine commandLine = cmdParser.parse(opts, args);

		if (commandLine.hasOption("accesslog")) {
			this.accesslogPath = commandLine.getOptionValue("accesslog");
		}

		if (commandLine.hasOption("startDate")) {
			this.startDate = DateTimeUtils.toArgumentsTime(commandLine.getOptionValue("startDate"));
		} else {
			throw new Exception("startDate argument is mandatory and should be in \"yyyy-MM-dd.HH:mm:ss\" format");
		}

		if (commandLine.hasOption("threshold")) {
			this.threshold = Integer.valueOf(commandLine.getOptionValue("threshold"));
		} else {
			throw new Exception("threshold argument is mandatory and should be an integer");
		}

		if (commandLine.hasOption("duration")) {
			if ("hourly".equals(commandLine.getOptionValue("duration"))) {
				this.duration = Duration.ofHours(1);
			} else if ("daily".equals(commandLine.getOptionValue("duration"))) {
				this.duration = Duration.ofDays(1);
			} else {
				throw new Exception("duration argument is mandatory and should be hourly or daily");
			}
		} else {
			throw new Exception("duration argument is mandatory and should be hourly or daily");
		}
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public Duration getDuration() {
		return duration;
	}

	public Integer getThreshold() {
		return threshold;
	}

	public String getAccesslogPath() {
		return accesslogPath;
	}

}
